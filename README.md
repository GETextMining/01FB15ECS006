Topic- Text Mining for Coversational Agents

For our project we are using platforms like Alexa (Alexa Skill Kit) and DialogFlow (API.AI) 
for building chat bots to answer a set of questions on a specific topic. We have chosen the
topic as “Apple”, "Google" and "GE"


This repository contains:

1) Alexa
- Intents 
- Utterances
- Lambda function

2) Google API 
- Intents 
- Utterances

Team Members

- Abhilash R Kashyap  01FB15ECS006
- Aishwarya P R       01FB15ECS020
- Anirudh S           01FB15ECS038
- Avinash V           01FB15ECS061