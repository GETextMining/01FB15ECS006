"""
This sample demonstrates a simple skill built with the Amazon Alexa Skills Kit.
The Intent Schema, Custom Slots, and Sample Utterances for this skill, as well
as testing instructions are located at http://amzn.to/1LzFrj6

For additional samples, visit the Alexa Skills Kit Getting Started guide at
http://amzn.to/1LGWsLG
"""

from __future__ import print_function


# --------------- Helpers that build all of the responses ----------------------

def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': "SessionSpeechlet - " + title,
            'content': "SessionSpeechlet - " + output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }


# --------------- Functions that control the skill's behavior ------------------

def get_welcome_response():
    """ If we wanted to initialize the session to have some attributes we could
    add those here
    """

    session_attributes = {}
    card_title = "Welcome"
    speech_output = "Welcome to the Alexa Skills Kit sample. " \
                    "Please tell me founder of apple." 
                    #"Founder of Apple is Jobs."
    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = "Please tell me founder of apple, " \
                    "Founder of Apple is Jobs."
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def handle_session_end_request():
    card_title = "Session Ended"
    speech_output = "Thank you for trying the Alexa Skills Kit sample. " \
                    "Have a nice day! "
    # Setting this to true ends the session and exits the skill.
    should_end_session = True
    return build_response({}, build_speechlet_response(
        card_title, speech_output, None, should_end_session))

"""
def create_favorite_color_attributes(favorite_color):
    return {"favoriteColor": favorite_color}


def set_color_in_session(intent, session):
    # Sets the color in the session and prepares the speech to reply to the
    #user.
    

    card_title = intent['name']
    session_attributes = {}
    should_end_session = False

    if 'Color' in intent['slots']:
        favorite_color = intent['slots']['Color']['value']
        session_attributes = create_favorite_color_attributes(favorite_color)
        speech_output = "I now know your favorite color is " + \
                        favorite_color + \
                        ". You can ask me your favorite color by saying, " \
                        "what's my favorite color?"
        reprompt_text = "You can ask me your favorite color by saying, " \
                        "what's my favorite color?"
    else:
        speech_output = "I'm not sure what your favorite color is. " \
                        "Please try again."
        reprompt_text = "I'm not sure what your favorite color is. " \
                        "You can tell me your favorite color by saying, " \
                        "my favorite color is red."
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def get_color_in_session(intent, session):
    session_attributes = {}
    reprompt_text = None
    
    if session.get('attributes', {}) and "favoriteColor" in session.get('attributes', {}):
        favorite_color = session['attributes']['favoriteColor']
        speech_output = "Your favorite color is " + favorite_color + \
                        ". Goodbye."
        should_end_session = True
    else:
    
    speech_output = "I'm not sure what your favorite color is. " \
                        "You can say, my favorite color is red."
    should_end_session = False

    # Setting reprompt_text to None signifies that we do not want to reprompt
    # the user. If the user does not respond or says something that is not
    # understood, the session will end.
    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))
"""

#def create_favorite_color_attributes(favorite_color):
#    return {"favoriteColor": favorite_color}

def apple1(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Please ask the appropriate question"
                    
    reprompt_text = "Please ask the appropriate question"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))
   
    
def apple(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Apple Inc. is an American multinational technology company headquartered in Cupertino,"\
"California that designs, develops, and sells consumer electronics, computer software, and"\
"online services."
                    
    reprompt_text ="Apple Inc. is an American multinational technology company headquartered in Cupertino,"\
"California that designs, develops, and sells consumer electronics, computer software, and"\
"online services."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

    """
    speech_output = "Founder of Apple was Steve Jobs"
                    
    reprompt_text = "Founder of Apple was Steve Jobs"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))
    """

def apple2(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "The company's hardware products include the iPhone smartphone, the iPad tablet"\
"computer, the Mac personal computer, the iPod portable media player, the Apple Watch"\
"smartwatch, the Apple TV digital media player, and the HomePod smart speaker."
                    
    reprompt_text = "The company's hardware products include the iPhone smartphone, the iPad tablet"\
"computer, the Mac personal computer, the iPod portable media player, the Apple Watch"\
"smartwatch, the Apple TV digital media player, and the HomePod smart speaker."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def apple3(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Apple&#39;s consumer software includes the macOS and iOS operating systems, the iTunes"\
"media player, the Safari web browser, and the iLife and iWork creativity and productivity"\
"suites."
                    
    reprompt_text = "The company&#39;s hardware products include the iPhone smartphone, the iPad tablet"\
"computer, the Mac personal computer, the iPod portable media player, the Apple Watch"\
"smartwatch, the Apple TV digital media player, and the HomePod smart speaker."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def apple4(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Its online services include the iTunes Store, the iOS App Store and Mac App Store, Apple"\
"Music, and iCloud."
                    
    reprompt_text = "Its online services include the iTunes Store, the iOS App Store and Mac App Store, Apple"\
"Music, and iCloud."
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple5(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Apple was founded by Steve Jobs, Steve Wozniak, and Ronald Wayne."
                    
    reprompt_text = "Apple was founded by Steve Jobs, Steve Wozniak, and Ronald Wayne."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple6(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "April 1976."
                    
    reprompt_text = "April 1976."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple7(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Microsoft Windows operating system is the competitor of Apple."
                    
    reprompt_text = "Microsoft Windows operating system is the competitor of Apple."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple8(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "For its lower-priced products."
                    
    reprompt_text = "For its lower-priced products."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple9(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Apple is the world's largest information technology company by revenue and the world's"\
"second-largest mobile phone manufacturer after Samsung."
                    
    reprompt_text = "Apple is the world's largest information technology company by revenue and the world's"\
"second-largest mobile phone manufacturer after Samsung."

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple10(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Apple Computer Company was founded on April 1, 1976, by Steve Jobs, Steve Wozniak and"\
"Ronald Wayne. The company&#39;s first product was the Apple I, a computer single-handedly"\
"designed and hand-built by Wozniak, and first shown to the public at the Homebrew"\
"Computer Club. Apple I was sold as a motherboard (with CPU, RAM, and basic textual-video"\
"chips), which was less than what is now considered a complete personal computer. The"\
"Apple I went on sale in July 1976 and was market-priced at $666.66 ($2,806 in 2016 dollars,"\
"adjusted for inflation)"
                    
    reprompt_text = "Apple Computer Company was founded on April 1, 1976, by Steve Jobs, Steve Wozniak and"\
"Ronald Wayne. The company&#39;s first product was the Apple I, a computer single-handedly"\
"designed and hand-built by Wozniak, and first shown to the public at the Homebrew"\
"Computer Club. Apple I was sold as a motherboard (with CPU, RAM, and basic textual-video"\
"chips), which was less than what is now considered a complete personal computer. The"\
"Apple I went on sale in July 1976 and was market-priced at $666.66 ($2,806 in 2016 dollars,"\
"adjusted for inflation)"

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def apple11(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "The company's first product was the Apple I"
                    
    reprompt_text = "The company's first product was the Apple I"
                    
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def apple12(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "The original Apple Watch smartwatch being introduced as a product with health and fitness-"\
"tracking."
                    
    reprompt_text = "The original Apple Watch smartwatch being introduced as a product with health and fitness-"\
"tracking."

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple13(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "On September 9, 2014"
                    
    reprompt_text = "On September 9, 2014"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple14(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Was announced by Tim Cook"
                    
    reprompt_text = "Was announced by Tim Cook"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple15(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Apple Watch was released on April 24, 2015."
                    
    reprompt_text = "Apple Watch was released on April 24, 2015."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple16(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "According to Steve Jobs, the company's name was inspired by his visit to an apple farm while"\
"on a fruitarian diet. Jobs thought the name Apple was fun, spirited and not intimidating."
                    
    reprompt_text = "According to Steve Jobs, the company's name was inspired by his visit to an apple farm while"\
"on a fruitarian diet. Jobs thought the name Apple was fun, spirited and not intimidating."

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple17(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Designed by Ron Wayne."
                    
    reprompt_text = "Designed by Ron Wayne."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple18(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Depicts Sir Isaac Newton sitting under an apple tree."
                    
    reprompt_text = "Depicts Sir Isaac Newton sitting under an apple tree."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple19(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Byte into an Apple"
                    
    reprompt_text = "Byte into an Apple"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def apple20(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "In the late 1970s"
                    
    reprompt_text = "In the late 1970s"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))
        
def google1(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Google LLC is an American multinational technology company"\
    "that specializes in Internet-related services and products"
                    
    reprompt_text = "Google LLC is an American multinational technology company"\
    "that specializes in Internet-related services and products"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))



def google2(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Online advertising technologies, search, cloud computing, software, and hardware"
                    
    reprompt_text = "Online advertising technologies, search, cloud computing, software, and hardware"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google3(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Google was founded in September 4, 1998"
                    
    reprompt_text = "Google was founded in September 4, 1998"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google4(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Google was founded  by Larry Page and Sergey Brin while"\
    "they were Ph.D. students at Stanford University, in California."
                    
    reprompt_text = "Google was founded  by Larry Page and Sergey Brin while"\
    "they were Ph.D. students at Stanford University, in California."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google5(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Google has its headquarters in Mountain View, California, nicknamed the Googleplex."
                    
    reprompt_text = "Google has its headquarters in Mountain View, California, nicknamed the Googleplex."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google6(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Sundar Pichai was appointed CEO of Google."
                    
    reprompt_text = "Sundar Pichai was appointed CEO of Google."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google7(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "The name of the search engine originated from a misspelling"\
    "of the word google, the number 1 followed by 100 zeros, which was picked to"\
    "signify that the search engine was intended to provide large quantities of information."
                    
    reprompt_text = "The name of the search engine originated from a misspelling"\
    "of the word google, the number 1 followed by 100 zeros, which was picked to"\
    "signify that the search engine was intended to provide large quantities of information."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google8(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "The domain name for Google was registered on September 15, 1997"
                    
    reprompt_text = "The domain name for Google was registered on September 15, 1997"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google9(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = " Craig Silverstein, a fellow PhD student at Stanford, was hired as the first employee."
                    
    reprompt_text = " Craig Silverstein, a fellow PhD student at Stanford, was hired as the first employee."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google10(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Google was initially funded by an August 1998 contribution of $100,000 from Andy Bechtolsheim,"\
    "co-founder of Sun Microsystems; the money was given before Google was incorporated."
                    
    reprompt_text = "Google was initially funded by an August 1998 contribution of $100,000 from Andy Bechtolsheim,"\
    "co-founder of Sun Microsystems; the money was given before Google was incorporated."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google11(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Google received money from three other angel investors in 1998: Amazon.com founder Jeff Bezos,"\
    "Stanford University computer science professor David Cheriton, and entrepreneur Ram Shriram."
                    
    reprompt_text = "Google received money from three other angel investors in 1998: Amazon.com founder Jeff Bezos,"\
    "Stanford University computer science professor David Cheriton, and entrepreneur Ram Shriram."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google12(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "As of 2016, Google owned and operated nine data centers across North and South America, "\
    "two in Asia, and four in Europe"
                    
    reprompt_text = "As of 2016, Google owned and operated nine data centers across North and South America, "\
    "two in Asia, and four in Europe"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google13(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Googles most efficient data center runs at 95 F  using only fresh air cooling"\
    "requiring no electrically powered air conditioning; the servers run so hot that humans cannot go near them"\
    "for extended periods."
                    
    reprompt_text = "Googles most efficient data center runs at 95 F using only fresh air cooling,"\
    "requiring no electrically powered air conditioning; the servers run so hot that humans cannot go near them"\
    "for extended periods."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google14(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "As of 2011, Google had about 900,000 servers in their data centers, based on energy usage"
                    
    reprompt_text = "As of 2011, Google had about 900,000 servers in their data centers, based on energy usage"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google15(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "On August 10, 2015, Google announced plans to reorganize its various interests as "\
    "a conglomerate called Alphabet."
                    
    reprompt_text = "On August 10, 2015, Google announced plans to reorganize its various interests as "\
    "a conglomerate called Alphabet."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google16(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Larry Page is the CEO of Alphabet."
                    
    reprompt_text = "Larry Page is the CEO of Alphabet."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google17(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "In 2011, 96% of Google's revenue was derived from its advertising programs."
                    
    reprompt_text = "In 2011, 96% of Google's revenue was derived from its advertising programs."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google18(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Google offers Gmail, and the newer variant Inbox, for email, Google Calendar"\
    "for time-management and scheduling, Google Maps for mapping, navigation and satellite imagery,"\
    "Google Drive for cloud storage of files,Google Docs, Sheets and Slides for productivity,Google "\
    "Photos for photo storage and sharing, Google Keep for note-taking, Google Translate for language"\
    "translation, YouTube for video viewing and sharing, and Google+, Allo, and Duo for social interaction"
                    
    reprompt_text = "Google offers Gmail, and the newer variant Inbox, for email, Google Calendar"\
    "for time-management and scheduling, Google Maps for mapping, navigation and satellite imagery,"\
    "Google Drive for cloud storage of files,Google Docs, Sheets and Slides for productivity,Google "\
    "Photos for photo storage and sharing, Google Keep for note-taking, Google Translate for language"\
    "translation, YouTube for video viewing and sharing, and Google+, Allo, and Duo for social interaction"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google19(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Google develops the Android mobile operating system, as well as its smartwatch,"\
    "television, car, and Internet of things-enabled smart devices variations. It also develops the "\
    "Google Chrome web browser, and Chrome OS, an operating system based on Chrome"
                    
    reprompt_text = "Google develops the Android mobile operating system, as well as its smartwatch,"\
    "television, car, and Internet of things-enabled smart devices variations. It also develops the "\
    "Google Chrome web browser, and Chrome OS, an operating system based on Chrome"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def google20(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "In January 2010, Google released Nexus One, the first Android phone under its own, Nexus, brand."\
    "In 2011, the Chromebook was introduced, described as a new kind of computer running Chrome OS. "\
    "In July 2013, Google introduced the Chromecast dongle, that allows users to stream content from their "\
    "smartphones to televisions. In June 2014, Google announced Google Cardboard, "\
    "a simple cardboard viewer that lets user place their smartphone in a special front compartment to"\
    "view virtual reality (VR) media. "
                    
    reprompt_text = "In January 2010, Google released Nexus One, the first Android phone under its own, Nexus, brand."\
    "In 2011, the Chromebook was introduced, described as a new kind of computer running Chrome OS. "\
    "In July 2013, Google introduced the Chromecast dongle, that allows users to stream content from their "\
    "smartphones to televisions. In June 2014, Google announced Google Cardboard, "\
    "a simple cardboard viewer that lets user place their smartphone in a special front compartment to"\
    "view virtual reality (VR) media. "
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge1(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "General Electric (GE) is an American multinational conglomerate corporation incorporated in New York. "
                    
    reprompt_text = "General Electric (GE) is an American multinational conglomerate corporation incorporated in New York. "
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge2(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "GE is headquartered in Boston, Massachusetts."
                    
    reprompt_text = "GE is headquartered in Boston, Massachusetts."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge3(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "The Nobel Prize has twice been awarded to employees of General Electric: Irving Langmuir in 1932 and Ivar Giaever in 1973."
                    
    reprompt_text = "The Nobel Prize has twice been awarded to employees of General Electric: Irving Langmuir in 1932 and Ivar Giaever in 1973."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge4(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "In 2017, GE ranked among the Fortune 500 as the thirteenth-largest firm in the U.S. by gross revenue."
                    
    reprompt_text = "In 2017, GE ranked among the Fortune 500 as the thirteenth-largest firm in the U.S. by gross revenue."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge5(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "In 2011, GE ranked as the 14th most profitable."
                    
    reprompt_text = "In 2011, GE ranked as the 14th most profitable."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge6(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = " Its main offices are located at 30 Rockefeller Plaza at Rockefeller"\
    "Center in New York City, known now as the Comcast Building."
                    
    reprompt_text = " Its main offices are located at 30 Rockefeller Plaza at Rockefeller"\
    "Center in New York City, known now as the Comcast Building."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge7(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "John L. Flannery is the current chief executive officer and chairman of the board of GE."\
    "He is the former president and chief executive of GE Healthcare."
                    
    reprompt_text = "John L. Flannery is the current chief executive officer and chairman of the board of GE."\
    "He is the former president and chief executive of GE Healthcare."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge8(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Jeffrey Immelt is the former CEO and former chairman of the board of GE. Previously, Immelt"\
    "had headed GEs Medical Systems division (now GE Healthcare) as its president and CEO."
                    
    reprompt_text = "Jeffrey Immelt is the former CEO and former chairman of the board of GE. Previously, Immelt"\
    "had headed GEs Medical Systems division (now GE Healthcare) as its president and CEO."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge9(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "GEs primary business divisions include GE Additive, Current, powered by GE, GE Lighting, GE Capital,"\
    "GE Aviation, GE Healthcare, GE Transportation, GE Global Research, GE Digital, GE Power, Baker, "\
    "Hughes, GE Renewable Energy"
                    
    reprompt_text = "GEs primary business divisions include GE Additive, Current, powered by GE, GE Lighting, GE Capital,"\
    "GE Aviation, GE Healthcare, GE Transportation, GE Global Research, GE Digital, GE Power, Baker, "\
    "Hughes, GE Renewable Energy"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge10(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Imagination at work"
                    
    reprompt_text = "Imagination at work"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def ge11(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "David Lucas"
                    
    reprompt_text = "David Lucas"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge12(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "We Bring Good Things to Life, which was used since 1979"
                    
    reprompt_text = "We Bring Good Things to Life, which was used since 1979"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge13(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "GE Inspira which is a customised font."
                    
    reprompt_text = "GE Inspira which is a customised font."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge14(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "In January 2017, General Electric signed an estimated $7 million deal"\
    "with the Boston Celtics to have its corporate logo put on the NBA teams jersey."
                    
    reprompt_text = "In January 2017, General Electric signed an estimated $7 million deal"\
    "with the Boston Celtics to have its corporate logo put on the NBA teams jersey."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge15(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "In 1889, Drexel, Morgan & Co., a company founded by J.P. Morgan and Anthony J. Drexel,"\
    "financed Edison's research and helped merge those companies under one corporation to form Edison General"\
    "Electric Company which was incorporated in New York on April 24, 1889."
                    
    reprompt_text = "In 1889, Drexel, Morgan & Co., a company founded by J.P. Morgan and Anthony J. Drexel,"\
    "financed Edison's research and helped merge those companies under one corporation to form Edison General"\
    "Electric Company which was incorporated in New York on April 24, 1889."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge16(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Over half of GEs revenue is derived from financial services."
                    
    reprompt_text = "Over half of GEs revenue is derived from financial services."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge17(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "With IBM (the largest), Burroughs, NCR, Control Data Corporation, Honeywell,"\
    "RCA and UNIVAC, GE was one of the eight major computer companies of the 1960s."
                    
    reprompt_text = "With IBM (the largest), Burroughs, NCR, Control Data Corporation, Honeywell,"\
    "RCA and UNIVAC, GE was one of the eight major computer companies of the 1960s."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge18(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "In 1927, Ernst Alexanderson of GE made the first demonstration of his"\
    "television broadcasts at his General Electric Realty Plot home at 1132 Adams Rd, Schenectady, NY."
                    
    reprompt_text ="In 1927, Ernst Alexanderson of GE made the first demonstration of his"\
    "television broadcasts at his General Electric Realty Plot home at 1132 Adams Rd, Schenectady, NY."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge19(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "GEs corporate headquarters was located in Fairfield, Connecticut (where it had been since 1974)."
                    
    reprompt_text = "GEs corporate headquarters was located in Fairfield, Connecticut (where it had been since 1974)."
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def ge20(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = True
    
    speech_output = "Thomas Edison"
                    
    reprompt_text = "Thomas Edison"
                    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


        
# --------------- Events ------------------

def on_session_started(session_started_request, session):
    """ Called when the session starts """

    print("on_session_started requestId=" + session_started_request['requestId']
          + ", sessionId=" + session['sessionId'])


def on_launch(launch_request, session):
    """ Called when the user launches the skill without specifying what they
    want
    """

    print("on_launch requestId=" + launch_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # Dispatch to your skill's launch
    return get_welcome_response()


def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """

    print("on_intent requestId=" + intent_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']
    
    """    
    # Dispatch to your skill's intent handlers
    if intent_name == "MyColorIsIntent":
        return set_color_in_session(intent, session)
    elif intent_name == "WhatsMyColorIntent":
        return get_color_from_session(intent, session)
    elif intent_name == "AMAZON.HelpIntent":
        return get_welcome_response()
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()
    else:
        raise ValueError("Invalid intent")
    """
    if intent_name == "apple":
        return apple(intent, session)
    elif intent_name == "appleOne":
        return apple1(intent,session)
    elif intent_name == "appleTwo":
        return apple2(intent,session)
    elif intent_name == "appleThree":
        return apple3(intent,session)
    elif intent_name == "appleFour":
        return apple4(intent,session)
    elif intent_name == "appleFive":
        return apple5(intent,session)
    elif intent_name == "appleSix":
        return apple6(intent,session)
    elif intent_name == "appleSeven":
        return apple7(intent,session)
    elif intent_name == "appleEight":
        return apple8(intent,session)
    elif intent_name == "appleNine":
        return apple9(intent,session)
    elif intent_name == "appleTen":
        return apple10(intent,session)
    elif intent_name == "appleEleven":
        return apple11(intent,session)
    elif intent_name == "appleTwelve":
        return apple12(intent,session)
    elif intent_name == "appleThirteen":
        return apple13(intent,session)
    elif intent_name == "appleFourteen":
        return apple14(intent,session)
    elif intent_name == "appleFifteen":
        return apple15(intent,session)
    elif intent_name == "appleSixteen":
        return apple16(intent,session)
    elif intent_name == "appleSeventeen":
        return apple17(intent,session)
    elif intent_name == "appleEighteen":
        return apple18(intent,session)
    elif intent_name == "appleNineteen":
        return apple19(intent,session)
    elif intent_name == "appleTwenty":
        return apple20(intent,session)
    elif intent_name == "googleOne":
        return google1(intent,session)
    elif intent_name == "googleTwo":
        return google2(intent,session)
    elif intent_name == "googleThree":
        return google3(intent,session)
    elif intent_name == "googleFour":
        return google4(intent,session)
    elif intent_name == "googleFive":
        return google5(intent,session)
    elif intent_name == "googleSix":
        return google6(intent,session)
    elif intent_name == "googleSeven":
        return google7(intent,session)
    elif intent_name == "googleEight":
        return google8(intent,session)
    elif intent_name == "googleNine":
        return google9(intent,session)
    elif intent_name == "googleTen":
        return google10(intent,session)
    elif intent_name == "googleEleven":
        return google11(intent,session)
    elif intent_name == "googleTwelve":
        return google12(intent,session)
    elif intent_name == "googleThirteen":
        return google13(intent,session)
    elif intent_name == "googleFourteen":
        return google14(intent,session)
    elif intent_name == "googleFifteen":
        return google15(intent,session)
    elif intent_name == "googleSixteen":
        return google16(intent,session)
    elif intent_name == "googleSeventeen":
        return google17(intent,session)
    elif intent_name == "googleEighteen":
        return google18(intent,session)
    elif intent_name == "googleNineteen":
        return google19(intent,session)
    elif intent_name == "googleTwenty":
        return google20(intent,session)
    elif intent_name == "geOne":
        return ge1(intent,session)
    elif intent_name == "geTwo":
        return ge2(intent,session)
    elif intent_name == "geThree":
        return ge3(intent,session)
    elif intent_name == "geFour":
        return ge4(intent,session)
    elif intent_name == "geFive":
        return ge5(intent,session)
    elif intent_name == "geSix":
        return ge6(intent,session)
    elif intent_name == "geSeven":
        return ge7(intent,session)
    elif intent_name == "geEight":
        return ge8(intent,session)
    elif intent_name == "geNine":
        return ge9(intent,session)
    elif intent_name == "geTen":
        return ge10(intent,session)
    elif intent_name == "geEleven":
        return ge11(intent,session)
    elif intent_name == "geTwelve":
        return g12(intent,session)
    elif intent_name == "geThirteen":
        return ge13(intent,session)
    elif intent_name == "geFourteen":
        return ge14(intent,session)
    elif intent_name == "geFifteen":
        return ge15(intent,session)
    elif intent_name == "geSixteen":
        return ge16(intent,session)
    elif intent_name == "geSeventeen":
        return ge17(intent,session)
    elif intent_name == "geEighteen":
        return ge18(intent,session)
    elif intent_name == "geNineteen":
        return ge19(intent,session)
    elif intent_name == "geTwenty":
        return ge20(intent,session)
    
        
def on_session_ended(session_ended_request, session):
    """ Called when the user ends the session.

    Is not called when the skill returns should_end_session=true
    """
    print("on_session_ended requestId=" + session_ended_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # add cleanup logic here


# --------------- Main handler ------------------

def lambda_handler(event, context):
    """ Route the incoming request based on type (LaunchRequest, IntentRequest,
    etc.) The JSON body of the request is provided in the event parameter.
    """
    print("event.session.application.applicationId=" +
          event['session']['application']['applicationId'])

    """
    Uncomment this if statement and populate with your skill's application ID to
    prevent someone else from configuring a skill that sends requests to this
    function.
    """
    # if (event['session']['application']['applicationId'] !=
    #         "amzn1.echo-sdk-ams.app.[unique-value-here]"):
    #     raise ValueError("Invalid Application ID")

    if event['session']['new']:
        on_session_started({'requestId': event['request']['requestId']},
                           event['session'])

    if event['request']['type'] == "LaunchRequest":
        return on_launch(event['request'], event['session'])
    elif event['request']['type'] == "IntentRequest":
        return on_intent(event['request'], event['session'])
    elif event['request']['type'] == "SessionEndedRequest":
        return on_session_ended(event['request'], event['session'])
